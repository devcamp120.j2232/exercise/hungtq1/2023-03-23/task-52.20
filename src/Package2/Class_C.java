package Package2;

import Package1.Class_A;

public class Class_C extends Class_A{ //it looks like the 'extends' does not work :)
    
    public static void main(String[] args) throws Exception {
    Class_A classA = new Class_A();
        classA.i = 1;
        classA.j = 2;
        classA.k = 3;
        classA.l = 4;
    }
}
